# Simulateur de division cellulaire

![Capture d'écran du programme](./Capture_d_écran_1.png)  
![Capture d'écran du programme](./Capture_d_écran_2.png)  

## Introduction

Ce logiciel simule la division cellulaire par les procédés simples d'auto-réplication. Il a uniquement un but éducatif et artistique.

## Fonctionnalités

 - Simulation de division cellulaire aléatoire sur fond blanc
 - Démonstration de la dérive génétique grâce aux couleurs

## Cas d'utilisation

Il peut servir à toute personne apprennat la programmation et souhaitant s'exercer à l'application des maths à l'algorithmique. En démontrant la dérive génétique des cellules via une hérédité entre cellule mère et fille, il permet un usage éducatif. Enfin, il peut aussi servir d'écran de veille aléatoire relaxant.

## Prérequis

 - [Python](https://www.python.org/)
 - Module Pygame pour python (`pip install pygame`)

## Installation

`pip install pygame`  
`git clone https://gitlab.univ-lille.fr/nina.durin.etu/simulation-de-division-cellulaire.git`  

## Démarrage rapide

`cd simulation-de-division-cellulaire`  
`python3 main.py`  

## Licence

`CC-0`  
Ce projet est sous licence libre CC-0, il peut donc être librement partagé, modifié, utilisé commercialement sans aucune attribution.
