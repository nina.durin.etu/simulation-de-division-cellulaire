import pygame # OBLIGATOIRE
from pygame.locals import * # OBLIGATOIRE
from random import randrange
from math import *

pygame.init() # OBLIGATOIRE

screen_info = pygame.display.Info()
# WIDTH, HEIGHT = screen_info.current_w, screen_info.current_h
WIDTH, HEIGHT = 500, 500
win = pygame.display.set_mode((WIDTH, HEIGHT))

win.fill((255,255,255))






class cell():
    ''' classe décrivant une cellule. '''
    def __init__(self, pos=(WIDTH/2, HEIGHT/2), color=None):
        self.size = 10 # rayon d'une cellule
        self.speed = (0,0) # vitesse
        self.pos = pos # position
        self.state = 0 # état de division
        self.delay = randrange(1000, 2000) # temps après lequel la cellule se divise
        self.color = (randrange(0,255),randrange(0,255),randrange(0,255)) if color is None else color

    def live(self): # vie de la celule
        self.state += 1
        if self.state >= self.delay: # si l'état est arrivé à la fin
            self.state = 0 # alors on redéfinit l'état à 0
            return 'divise' # alors on divise la cellule
        self.size = 10 + int(self.state / 200)

    def place(self): # s'occuppe de replacer la cellule en fonction des collisions
        self.pos = (self.pos[0] + self.speed[0], self.pos[1] + self.speed[1])
        self.speed = (self.speed[0] * 0.95, self.speed[1] * 0.95)
        pass

    def alter_color(self): # Change légèrement la couleur en faisant attention à ne pas sortir des limites 0-255
        current_color = list(self.color)
        for i in range(3):
            current_color[i] += randrange(-5, 6, 1)
            if current_color[i] > 255:
                current_color[i] = 255
            elif current_color[i] < 0:
                current_color[i] = 0
        self.color = tuple(current_color)




cells = [cell()]

continuer = True
while continuer:

    for elt in cells:
        if elt.live() == 'divise': # on run la fonction live et on vérifie en plus si elle renvoie 'divise'
            ### On divise la celule concernée
            cells.append(cell( (elt.pos[0]+randrange(-200,201)/100, elt.pos[1]+randrange(-200,201)/100), elt.color )) # on ajoute une cellule avec une position légerement excentrée et une couleur héritée de la précédente
            cells[-1].alter_color()
            pass

    ### gestion de la collision
    for i in cells:
        for j in cells:
            if j != i:
                delta_x = i.pos[0] - j.pos[0]
                delta_y = i.pos[1] - j.pos[1]
                distance = sqrt( (delta_x)**2 + (delta_y)**2 )
                if distance < (i.size + j.size): # cas d'une collision : si les deux collisionnent, on fait : 
                    i.speed = (i.speed[0] + ( delta_x*(0.01/distance)), i.speed[1] + ( delta_y)*(0.01/distance))
                    j.speed = (j.speed[0] + (-delta_x*(0.01/distance)), j.speed[1] + (-delta_y)*(0.01/distance))
                    pass
                pass
            pass
        pass

    ### Affichage
    win.fill((255,255,255))
    for elt in cells:
        elt.place()
        pygame.draw.circle(win, elt.color, (int(elt.pos[0]), int(elt.pos[1])), elt.size,)
        pygame.draw.circle(win, (50, 50, 50), (int(elt.pos[0]), int(elt.pos[1])), elt.size, 1)
        pass

    ### Suppression des cellules en trop
    if len(cells) >= 100:
        cells = cells[2:98]

    pygame.display.flip() # Rafraichissement de la fenêtre.


    ### Gestion des évenements
    for event in pygame.event.get(): # on parcourt la liste de tous les évenements reçus.
        if event.type == KEYDOWN:
            if event.key == K_ESCAPE: # si on tape sur la touche echap
                continuer = False # on arête la boucle.
        if event.type == QUIT: # si un des évenements est de type QUIT
            continuer = False # on arête la boucle.
            pass
        pass


    continue

print("Programme exécuté sans erreurs")
